import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocialService } from '../../../api/social.service';
import {Post} from '../../../models/post';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss'],
})

export class PostListComponent implements OnInit {
  private posts: [Post];

  constructor(
    public restApi: SocialService,
    public router: Router,
  ) {
  }

  loadPosts(): void {
    this.restApi.getPosts().subscribe((data) => {
      this.posts = data;
    });

  }

  ngOnInit() {
    this.loadPosts();
  }

}
