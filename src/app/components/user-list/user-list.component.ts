import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocialService } from '../../../api/social.service';
import { User } from '../../../models/user';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})

export class UserListComponent implements OnInit {
  private users: [User];

  displayedColumns = ['Id', 'Nombre', 'Apellido', 'Nacionalidad', 'Edad'];
  dataSource: any;

  constructor(
    public restApi: SocialService,
    public router: Router,
  ) {
  }

  loadUsers(): void {
    debugger;
    this.restApi.getUsers().subscribe((data) => {

      this.dataSource = new MatTableDataSource();
      this.dataSource.data = data;
      console.log(this.dataSource.data);

    });

  }

  ngOnInit() {
    this.loadUsers();
  }

}
