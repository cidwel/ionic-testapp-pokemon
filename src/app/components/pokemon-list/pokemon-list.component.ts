import { Component, OnInit } from '@angular/core';
import { Pokemon } from './models/pokemon';
import { padWithZeroes } from 'src/utils/general';
import { Router } from '@angular/router';
import {PokemonService} from '../../../api/pokemon.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss'],
})

export class PokemonListComponent implements OnInit {

  selectedPokemon: Pokemon;
  pokemons: {
    isPrototypeOf(v: Object): boolean;
    hasOwnProperty(v: PropertyKey): boolean;
    propertyIsEnumerable(v: PropertyKey): boolean;
    valueOf(): Object;
    constructor: Function;
    toString(): string;
    id: any;
    toLocaleString(): string }[] = [];

  constructor(
    public restApi: PokemonService,
    public router: Router,
  ) {
  }

  loadPokemons(): void {
    this.restApi.getPokemons().subscribe((data) => {
      this.pokemons = data.results.map((pokeData, index) => ({
        ...pokeData,
        id: padWithZeroes(index + 1, 3),
      }));
    });

  }

  ngOnInit() {
    this.loadPokemons();
  }

  onSelect(pokemon: Pokemon): void {
    this.selectedPokemon = pokemon;
    this.router.navigate(['/tabs/tab2', pokemon.name]);

  }
}
