import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokemonService } from 'src/api/pokemon.service';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.scss'],
})

export class PokemonComponent implements OnInit {

  pokemon: {};
  name: string;

  constructor(
    public restApi: PokemonService,
    public route : ActivatedRoute,
  ) {
  }

  loadPokemon(name): void {
    this.restApi.getPokemon(name).subscribe((data: {}) => this.pokemon = data);
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.name = params['name'];
      this.loadPokemon(this.name);
    });
  }

}
