export class Pokemon {
  name: string;
  url: string;
  id: string;
  base_experience: number;
  stats: object;
  weight: number;
  abilities: [object];
  moves: [object];
  sprites: [object];
}
