import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Post } from 'src/models/post';
import {User} from '../models/user';

@Injectable({
  providedIn: 'root',
})

export class SocialService {

  // Define API
  apiURL = 'https://jsonplaceholder.typicode.com';

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  getPosts(): Observable<[Post]> {
    return this.http.get<[Post]>(`${this.apiURL}/posts`)
      .pipe(
        retry(1),
        catchError(this.handleError),
      );
  }

  getUsers(): Observable<[User]> {
    return this.http.get<[User]>(`${this.apiURL}/users`)
      .pipe(
        retry(1),
        catchError(this.handleError),
      );
  }

  // Error handling
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

}
