import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Pokemon } from '../models/pokemon';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import {ApiResult} from 'src/models/apiResult';

@Injectable({
  providedIn: 'root',
})

export class PokemonService {

  // Define API
  apiURL = 'https://pokeapi.co/api/v2/';

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  // HttpClient API get() method => Fetch employees list
  getPokemons(limit = 151): Observable<ApiResult> {
    return this.http.get<ApiResult>(`${this.apiURL}pokemon/?limit=${limit}`)
    .pipe(
      retry(1),
      catchError(this.handleError),
    );
  }

  /**
   * Lista de pokemons
   * @param {string} Nombre de pokemon
   */
  getPokemon(name): Observable<ApiResult> {
    return this.http.get<ApiResult>(`${this.apiURL}pokemon/${name}`)
    .pipe(
      retry(1),
      catchError(this.handleError),
    );
  }

  // Error handling
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
    // Get client-side error
      errorMessage = error.error.message;
    } else {
    // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

}
